﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MofacInteractiveBoard.Model
{
    public class LogTable
    {
        public string CurrentTime { get; set; }
        public string Content { get; set; }
    }
}
