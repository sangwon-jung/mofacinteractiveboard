﻿using MofacInteractiveBoard.Model;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MofacInteractiveBoard
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        private SerialPort _serialPort { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            VersionControl.BuildCheck(RES.BUILD_NUM, RES.BUILD_DATE);

            /// ComboBox 셋팅
            /// 포트가 연결되어있지 않으면 메세지 출력
            ComboBox_Port.ItemsSource = SerialPort.GetPortNames();
            if (SerialPort.GetPortNames().Length < 0 )
            {
                MessageBox.Show("Connect the Arduino");
                return;
            }
            ComboBox_Port.SelectedIndex = 0; 
            _serialPort = new SerialPort();
            AButton.IsEnabled = false;
            BButton.IsEnabled = false;
            CButton.IsEnabled = false;
        }

        
        /// <summary>
        /// 1) 연결 시작 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectionButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (!_serialPort.IsOpen)
            {
                _serialPort.PortName = ComboBox_Port.Text;
                _serialPort.BaudRate = 9600;  
                _serialPort.DataBits = 8;
                _serialPort.StopBits = StopBits.One;
                _serialPort.Parity = Parity.None;
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceived); 
                _serialPort.DtrEnable = true;
                _serialPort.RtsEnable = true;
                _serialPort.Open();
                UpdateUI($"{ComboBox_Port.Text} has been opened.");
                ComboBox_Port.IsEnabled = false;

                AButton.IsEnabled = true;
                BButton.IsEnabled = true;
                CButton.IsEnabled = true;
            }
            else
            {
                UpdateUI("The port is already open.");
            }
        }

        /// <summary>
        /// 1) 연결 해제 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisconnectButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (_serialPort.IsOpen)
            {
                _serialPort.Close();

                UpdateUI("Close Port");
                ComboBox_Port.IsEnabled = true;
                AButton.IsEnabled = false;
                BButton.IsEnabled = false;
                CButton.IsEnabled = false;
            }
            else
            {
                UpdateUI("The port is already closed.");
            }

        }

        /// <summary>
        /// 수신 이벤트 발생
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
        }

        /// <summary>
        /// 1) 메세지 입력후 전달
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendButton_Clicked(object sender, RoutedEventArgs e)
        {
            UpdateUI("Send : "+TextBox_SendMessage.Text);
            _serialPort.Write(TextBox_SendMessage.Text);
        }

     
        /// <summary>
        /// 1) 원터치 전달 버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AButton_Clicked(object sender, RoutedEventArgs e)
        {
            UpdateUI("A Button Send : a");
            _serialPort.Write("a");
        }

        private void BButton_Clicked(object sender, RoutedEventArgs e)
        {
            UpdateUI("B Button Send : b");
            _serialPort.Write("b");
        }

        private void CButton_Clicked(object sender, RoutedEventArgs e)
        {
            UpdateUI("C Button Send : c");
            _serialPort.Write("c");
        }

        #region UI

        private List<string> resultValueList = new List<string>();
        public void ResultFile(string result)
        {
            this.resultValueList.Add(result);
        }


        public void UpdateUI(string Content)
        {
            // (5) UI Update
            this.Dispatcher.BeginInvoke(
                new Action(() =>
                {
                    LogLV.Items.Add(new LogTable
                    {
                        CurrentTime = DateTime.Now.ToString(),
                        //Content = Encoding.UTF8.GetString(dgram)
                        Content = Content
                    });

                    var scrollViewer = GetScrollViewer(LogLV) as ScrollViewer;
                    if (scrollViewer != null)
                    {
                        scrollViewer.ScrollToBottom();
                    }
                }));
        }


        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer

            if (o is ScrollViewer)
            { return o; }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result == null)
                {
                    continue;
                }
                else
                {
                    return result;
                }
            }
            return null;
        }
        #endregion

    }
}
